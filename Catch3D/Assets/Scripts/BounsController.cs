using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounsController : MonoBehaviour
{
    GameObject player;
    public float getSpeed = -0.5f;

    // Start is called before the first frame update
    void Start()
    {
        this.player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, 0, this.getSpeed);

        if (transform.position.x < -12.0f)
        {
            Destroy(gameObject);
        }
        
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "Player")
        {
            Destroy(gameObject);
        }
    }
}
   
