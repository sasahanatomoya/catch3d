using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGController : MonoBehaviour
{
    public float getSpeed = -0.5f;
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, 0, this.getSpeed);

        if (transform.position.z < -12.0f)
        {
            Destroy(gameObject);
        }
    }
}
