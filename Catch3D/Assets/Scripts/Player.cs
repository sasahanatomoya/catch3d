using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour
{
    GameObject manager;

    void Start()
    {
        this.manager = GameObject.Find("GameManager");
    }
    
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Good")
        {
            //Debug.Log("Good");
            GameObject manager = GameObject.Find("GameManager");
            manager.GetComponent<GameManager>().RecoveryHp();
            this.manager.GetComponent<GameManager>().GetGood();
        }
        else if(other.gameObject.tag == "Bouns")
        {
            GameObject manager = GameObject.Find("GameManager");
            manager.GetComponent<GameManager>().BounsHp();
            this.manager.GetComponent<GameManager>().GetBouns();
        }
        else
        { 
            //Debug.Log("Bad");
            GameObject manager = GameObject.Find("GameManager");
            manager.GetComponent<GameManager>().DecreaseHp();
            this.manager.GetComponent<GameManager>().GetBad();          
        }       
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            transform.Translate(-1, 0, 0);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            transform.Translate(1, 0, 0);
        }
    }
}
