using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GBGenerator : MonoBehaviour
{
    public GameObject goodPrefab;
    public GameObject badPrefab;
    public GameObject bounsPrefab;
    float span = 0.2f;
    float delta = 0;
    int ratio = 9;
    //float speed = -0.05f;
    
    //public void SetParameter(float span, float speed, int ratio)
    //{
    //    this.span = span;
    //    this.speed = speed;
    //    this.ratio = ratio;
    //}

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if(this.delta > this.span)
        {
            this.delta = 0;
            GameObject item;
            int dice = Random.Range(1, 21);
            if (dice >= this.ratio)
            { 
                item = Instantiate(goodPrefab) as GameObject;
            }
            else if(dice == 1)
            {                
                item = Instantiate(bounsPrefab) as GameObject;
            }
            else
            {
                item = Instantiate(badPrefab) as GameObject;
            }
            int px = Random.Range(-8, 8);
            item.transform.position = new Vector3(px, 0, 20);
            //item.GetComponent<GBController>().getSpeed = this.speed;
            //item.GetComponent<BounsController>().getSpeed = this.speed;
        }
    }
}
