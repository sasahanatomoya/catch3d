using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGGenerator : MonoBehaviour
{
    public GameObject bill1Prefab;
    public GameObject bill2Prefab;
    public GameObject house1Prefab;
    public GameObject gasstaitonPrefab;
    float span = 0.25f;
    float delta = 0;
    int ratio = 9;


    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        { 
            this.delta = 0;
            GameObject item;
            int dice = Random.Range(1, 11);
            if (dice >= this.ratio)
            {
                item = Instantiate(bill1Prefab) as GameObject;
            }
            else if (this.ratio <= 9)
            {
                item = Instantiate(bill2Prefab) as GameObject;
            }
            else if (this.ratio <= 5)
            {
                item = Instantiate(house1Prefab) as GameObject;
            }
            else
            {
                item = Instantiate(gasstaitonPrefab) as GameObject;
            }
            item.transform.position = new Vector3(-25, 0, 90);
        }
    }
}
