using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGGenerator2 : MonoBehaviour
{
    public GameObject shopPrefab;
    public GameObject house2Prefab;
    public GameObject bill3Prefab;
    float span = 0.25f;
    float delta = 0;
    int ratio = 8;

    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        { 
            this.delta = 0;
            GameObject item;
            int dice = Random.Range(1, 11);
            if (dice >= this.ratio)
            {
                item = Instantiate(shopPrefab) as GameObject;
            }
            else if (this.ratio <= 5)
            {
                item = Instantiate(house2Prefab) as GameObject;
            }
            else
            {
                item = Instantiate(bill3Prefab) as GameObject;
            }
            item.transform.position = new Vector3(7, 0, 90);
        }
    }
}
