using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGLoop : MonoBehaviour
{
    private float speed = 50;

    void Update()
    {
        transform.position -= new Vector3(0, 0, Time.deltaTime * speed);

        if (transform.position.z <= -30f)
        {
            transform.position = new Vector3(0, 0, 200);
        }
    }
}
