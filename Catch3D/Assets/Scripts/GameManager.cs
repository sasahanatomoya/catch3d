using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    GameObject HpGauge;
    GameObject ScoreText;
    GameObject TimeText;
    GameObject GBGenerator;
    int point = 0;
    int hpflag = 10;
    float time = 30.0f;

    public void GetGood()
    {
        this.point += 1000;
    }
    public void GetBad()
    {
        this.point -= 1000;
    }
    public void GetBouns()
    {
        this.point += 10000;
    }

    // Start is called before the first frame update
    void Start()
    {
        this.HpGauge = GameObject.Find("HpGauge");
        this.ScoreText = GameObject.Find("ScoreText");
        this.TimeText = GameObject.Find("TimeText");
        this.GBGenerator = GameObject.Find("GBGenerator");
    }

    public void DecreaseHp()
    {
        this.HpGauge.GetComponent<Image>().fillAmount -= 0.2f;
        hpflag -= 2;
        //Debug.Log(hpflag);
        if(hpflag == 0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }

    public void RecoveryHp()
    {
        this.HpGauge.GetComponent<Image>().fillAmount += 0.1f;
        if(hpflag <= 10)
        {
            //Debug.Log(hpflag);
            hpflag += 1;
        }
    }

    public void BounsHp()
    {
        this.HpGauge.GetComponent<Image>().fillAmount += 0.5f;
        if (hpflag <= 10)
        {
            //Debug.Log(hpflag);
            hpflag += 5;
        }
    }

    void Update()
    {       
        this.time -= Time.deltaTime;

        //if(this.time < 0)
        //{
        //    this.time = 0;
        //    this.GBGenerator.GetComponent<GBGenerator>().SetParameter(10000.0f, 0, 0);
        //}
        //else if (0 <= this.time && this.time< 5)
        //{
        //    this.GBGenerator.GetComponent<GBGenerator>().SetParameter(0.9f, -0.25f, 3);
        //}
        //else if (0 <= this.time && this.time < 10)
        //{
        //    this.GBGenerator.GetComponent<GBGenerator>().SetParameter(0.4f, -0.45f, 6);
        //}
        //else if (0 <= this.time && this.time < 20)
        //{
        //    this.GBGenerator.GetComponent<GBGenerator>().SetParameter(0.7f, -0.25f, 4);
        //}
        //else if (0 <= this.time && this.time < 30)
        //{
        //    this.GBGenerator.GetComponent<GBGenerator>().SetParameter(1.0f, -0.15f, 2);
        //}

        this.TimeText.GetComponent<Text>().text = this.time.ToString("F1");
        this.ScoreText.GetComponent<Text>().text = this.point.ToString() + "";

        if(this.time <= 0)
        {
            if(this.point >= 10000)
            {
                SceneManager.LoadScene("GameClearScene");
            }
            else if(this.point < 10000)
            {
                SceneManager.LoadScene("GameOverScene");
            }
        }
        if (this.point >= 50000)
        {
            SceneManager.LoadScene("GameClearScene");
        }


    }
}
